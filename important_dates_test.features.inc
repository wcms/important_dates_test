<?php
/**
 * @file
 * important_dates_test.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function important_dates_test_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function important_dates_test_node_info() {
  $items = array(
    'important_dates_test' => array(
      'name' => t('Important dates test'),
      'base' => 'node_content',
      'description' => t('Testing of important dates'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
