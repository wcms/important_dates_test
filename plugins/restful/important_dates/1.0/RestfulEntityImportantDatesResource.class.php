<?php

/**
 * @file
 */

/**
 *
 */
class RestfulEntityImportantDatesResource extends RestfulEntityBaseNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    unset($public_fields['label']);

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['type'] = array(
      'property' => 'field_uw_id_type',
    );

    $public_fields['description'] = array(
      'property' => 'field_uw_id_description',
    );

    return $public_fields;
  }
}
