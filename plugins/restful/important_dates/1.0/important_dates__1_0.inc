<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Important Dates'),
  'resource' => 'important dates',
  'name' => 'important_dates__1_0',
  'entity_type' => 'node',
  'bundle' => 'important_dates_test',
  'description' => t('Export the important dates content type.'),
  'class' => 'RestfulEntityImportantDatesResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);

